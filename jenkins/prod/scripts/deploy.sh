#!/usr/bin/env bash

scp ./target/utilitiesone-1.0.0-SNAPSHOT.jar utilitiesone@165.227.85.149:/home/utilitiesone/apps

ssh utilitiesone@165.227.85.149 "chmod 755 /home/utilitiesone/apps/utilitiesone-1.0.0-SNAPSHOT.jar"

ssh utilitiesone@165.227.85.149 "sudo ln -sf /home/utilitiesone/apps/utilitiesone-1.0.0-SNAPSHOT.jar /etc/init.d/utilitiesone"

ssh utilitiesone@165.227.85.149 "sudo systemctl daemon-reload"
ssh utilitiesone@165.227.85.149 "sudo systemctl stop utilitiesone"
ssh utilitiesone@165.227.85.149 "sudo systemctl start utilitiesone"
