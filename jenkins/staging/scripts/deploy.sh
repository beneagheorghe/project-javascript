#!/usr/bin/env bash

sudo mkdir -p /home/dev/apps/dev.utilitiesone.com
sudo cp ./target/utilitiesone-1.0.0-SNAPSHOT.jar /home/dev/apps/dev.utilitiesone.com
sudo chmod 755 /home/dev/apps/dev.utilitiesone.com/utilitiesone-1.0.0-SNAPSHOT.jar

if [ ! -L "/etc/init.d/utilitiesone" ]; then
    sudo ln -s /home/dev/apps/dev.utilitiesone.com/utilitiesone-1.0.0-SNAPSHOT.jar /etc/init.d/utilitiesone
fi

sudo systemctl daemon-reload
sudo systemctl stop utilitiesone
sleep 1
sudo systemctl start utilitiesone