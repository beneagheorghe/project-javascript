<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <style>
        p {
            font-size: 16px;
        }

        table {
            font-family: 'Montserrat';
            font-size: 18px;
            border-collapse: collapse;
            width: 50%;
            background-color: #f3f3f3;
        }

        td {
            border: 1px solid #ffffff;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>

<table>
    <tr>
        <td>Job Title:</td>
        <td>
            <#if jobTitle?has_content>
                ${jobTitle}
            <#else>
                Not indicated
            </#if>
        </td>
    </tr>
    <tr>
        <td>Name:</td>
        <td>
            <#if fullName?has_content>
                ${fullName}
            <#else>
                Not indicated
            </#if>
        </td>
    </tr>
    <tr>
        <td>Email:</td>
        <td>
            <#if email?has_content>
                ${email}
            <#else>
                Not indicated
            </#if>
        </td>
    </tr>
    <tr>
        <td>Phone:</td>
        <td>
            <#if phone?has_content>
                ${phone}
            <#else>
                Not indicated
            </#if>
        </td>
    </tr>
    <tr>
        <td>Message:</td>
        <td>
            <#if message?has_content>
                ${message}
            <#else>
                Not indicated
            </#if>
        </td>
    </tr>
</table>

</body>
</html>