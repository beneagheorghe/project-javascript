document.addEventListener('DOMContentLoaded', function () {

    if(location.pathname === '/projects') {
        showLocations();
    }
});

function showLocations() {

    $("div.enevtItm").removeClass("enevtItmActive");

    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 39.9622601, lng: -83.0007065},
        zoom: 5,
        streetViewControl: false,
        styles: styles
    });

    addMarker({lat: 39.9622601, lng: -83.0007065}, map, iconConstruction,"<img src='./images/map/pop-const-1.png'>");
    addMarker({lat: 41.765770, lng: -72.673360}, map, iconConstruction,"<img src='./images/map/pop-const-2.png'>");
    addMarker({lat: 41.6648216, lng: -72.6392587}, map, iconConstruction,"<img src='./images/map/pop-const-3.png'>");
    addMarker({lat: 41.8755616, lng: -87.6244212}, map, iconConstruction,"<img src='./images/map/pop-const-4.png'>");
    addMarker({lat: 47.6038321, lng: -122.3300624}, map, iconConstruction,"<img src='./images/map/pop-const-5.png'>");
    addMarker({lat: 39.9527237, lng: -75.1635262}, map, iconConstruction,"<img src='./images/map/pop-const-6.png'>");
    addMarker({lat: 27.950575, lng: -82.457176}, map, iconConstruction,"<img src='./images/map/pop-const-7.png'>");
    addMarker({lat: 37.431572, lng: -78.656891}, map, iconConstruction,"<img src='./images/map/pop-const-8.png'>");
    addMarker({lat: 31.968599, lng: -99.901810}, map, iconConstruction,"<img src='./images/map/pop-const-9.png'>");
    addMarker({lat: 35.517490, lng: -86.580444}, map, iconConstruction,"<img src='./images/map/pop-const-10.png'>");
    addMarker({lat: 38.597626, lng: -80.454903}, map, iconConstruction,"<img src='./images/map/pop-const-11.png'>");

    addMarker({lat: 40.440624, lng: -79.995888}, map, iconWireless, "<img src='./images/map/pop-wir-1.png'>");
    addMarker({lat: 40.267193, lng: -86.134903}, map, iconWireless, "<img src='./images/map/pop-wir-2.png'>");
    addMarker({lat: 42.032974, lng: -93.581543}, map, iconWireless, "<img src='./images/map/pop-wir-3.png'>");
    addMarker({lat: 41.878113, lng: -87.629799}, map, iconWireless, "<img src='./images/map/pop-wir-4.png'>");

    addMarker({lat: 41.203323, lng: -77.194527}, map, iconEnergy,"<img src='./images/map/pop-energy-1.png'>");
    addMarker({lat: 41.878113, lng: -87.629799}, map, iconEnergy,"<img src='./images/map/pop-eng-2.png'>");

    addMarker({lat: 41.203323, lng: -77.194527}, map, iconEngineering,"<img src='./images/map/pop-eng-1.png'>");

    addMarker({lat: 38.627003, lng: -90.199402}, map, iconTechnologyDeployment,"<img src='./images/map/pop-td-1.png'>");
    addMarker({lat: 40.440624, lng: -79.995888}, map, iconTechnologyDeployment,"<img src='./images/map/pop-td-2.png'>");
    addMarker({lat: 39.099728, lng: -94.578568}, map, iconTechnologyDeployment,"<img src='./images/map/pop-td-3.png'>");
    addMarker({lat: 39.952583, lng: -75.165222}, map, iconTechnologyDeployment,"<img src='./images/map/pop-td-4.png'>");
    addMarker({lat: 39.802820, lng: -75.055680}, map, iconTechnologyDeployment,"<img src='./images/map/pop-td-5.png'>");
    addMarker({lat: 42.360081, lng: -71.058884}, map, iconTechnologyDeployment,"<img src='./images/map/pop-td-6.png'>");
    addMarker({lat: 40.712776, lng: -74.005974}, map, iconTechnologyDeployment,"<img src='./images/map/pop-td-7.png'>");
    addMarker({lat: 43.156578, lng: -77.608849}, map, iconTechnologyDeployment,"<img src='./images/map/pop-td-8.png'>");
    addMarker({lat: 42.886448, lng: -78.878372}, map, iconTechnologyDeployment,"<img src='./images/map/pop-td-9.png'>");
    addMarker({lat: 41.878113, lng: -87.629799}, map, iconTechnologyDeployment,"<img src='./images/map/pop-td-10.png'>");
    addMarker({lat: 39.103119, lng: -84.512016}, map, iconTechnologyDeployment,"<img src='./images/map/pop-td-11.png'>");
}

function locationsByProjectType(projectType) {

    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 39.9622601, lng: -83.0007065},
        zoom: 5,
        streetViewControl: false,
        styles: styles
    });

    switch (projectType) {
        case 'construction':
            addMarker({lat: 39.9622601, lng: -83.0007065}, map, iconConstruction,"<img src='./images/map/pop-const-1.png'>");
            addMarker({lat: 41.765770, lng: -72.673360}, map, iconConstruction,"<img src='./images/map/pop-const-2.png'>");
            addMarker({lat: 41.6648216, lng: -72.6392587}, map, iconConstruction,"<img src='./images/map/pop-const-3.png'>");
            addMarker({lat: 41.8755616, lng: -87.6244212}, map, iconConstruction,"<img src='./images/map/pop-const-4.png'>");
            addMarker({lat: 47.6038321, lng: -122.3300624}, map, iconConstruction,"<img src='./images/map/pop-const-5.png'>");
            addMarker({lat: 39.9527237, lng: -75.1635262}, map, iconConstruction,"<img src='./images/map/pop-const-6.png'>");
            addMarker({lat: 27.950575, lng: -82.457176}, map, iconConstruction,"<img src='./images/map/pop-const-7.png'>");
            addMarker({lat: 37.431572, lng: -78.656891}, map, iconConstruction,"<img src='./images/map/pop-const-8.png'>");
            addMarker({lat: 31.968599, lng: -99.901810}, map, iconConstruction,"<img src='./images/map/pop-const-9.png'>");
            addMarker({lat: 35.517490, lng: -86.580444}, map, iconConstruction,"<img src='./images/map/pop-const-10.png'>");
            addMarker({lat: 38.597626, lng: -80.454903}, map, iconConstruction,"<img src='./images/map/pop-const-11.png'>");
            break;
        case 'wireless':
            map.setCenter({lat: 41.878113, lng: -87.629799});
            map.zoom = 6;
            addMarker({lat: 40.440624, lng: -79.995888}, map, iconWireless, "<img src='./images/map/pop-wir-1.png'>");
            addMarker({lat: 40.267193, lng: -86.134903}, map, iconWireless, "<img src='./images/map/pop-wir-2.png'>");
            addMarker({lat: 42.032974, lng: -93.581543}, map, iconWireless, "<img src='./images/map/pop-wir-3.png'>");
            addMarker({lat: 41.878113, lng: -87.629799}, map, iconWireless, "<img src='./images/map/pop-wir-4.png'>");
            break;
        case 'energy':
            map.setCenter({lat: 41.203323, lng: -77.194527});
            map.zoom = 6;
            addMarker({lat: 41.203323, lng: -77.194527}, map, iconEnergy,"<img src='./images/map/pop-energy-1.png'>");
            addMarker({lat: 41.878113, lng: -87.629799}, map, iconEnergy,"<img src='./images/map/pop-eng-2.png'>");
            break;
        case 'engineering':
            map.setCenter({lat: 41.203323, lng: -77.194527});
            map.zoom = 6;
            addMarker({lat: 41.203323, lng: -77.194527}, map, iconEngineering,"<img src='./images/map/pop-eng-1.png'>");
            break;
        case 'technology-deployment':
            map.setCenter({lat: 38.627003, lng: -90.199402});
            map.zoom = 5;
            addMarker({lat: 38.627003, lng: -90.199402}, map, iconTechnologyDeployment,"<img src='./images/map/pop-td-1.png'>");
            addMarker({lat: 40.440624, lng: -79.995888}, map, iconTechnologyDeployment,"<img src='./images/map/pop-td-2.png'>");
            addMarker({lat: 39.099728, lng: -94.578568}, map, iconTechnologyDeployment,"<img src='./images/map/pop-td-3.png'>");
            addMarker({lat: 39.952583, lng: -75.165222}, map, iconTechnologyDeployment,"<img src='./images/map/pop-td-4.png'>");
            addMarker({lat: 39.802820, lng: -75.055680}, map, iconTechnologyDeployment,"<img src='./images/map/pop-td-5.png'>");
            addMarker({lat: 42.360081, lng: -71.058884}, map, iconTechnologyDeployment,"<img src='./images/map/pop-td-6.png'>");
            addMarker({lat: 40.712776, lng: -74.005974}, map, iconTechnologyDeployment,"<img src='./images/map/pop-td-7.png'>");
            addMarker({lat: 43.156578, lng: -77.608849}, map, iconTechnologyDeployment,"<img src='./images/map/pop-td-8.png'>");
            addMarker({lat: 42.886448, lng: -78.878372}, map, iconTechnologyDeployment,"<img src='./images/map/pop-td-9.png'>");
            addMarker({lat: 41.878113, lng: -87.629799}, map, iconTechnologyDeployment,"<img src='./images/map/pop-td-10.png'>");
            addMarker({lat: 39.103119, lng: -84.512016}, map, iconTechnologyDeployment,"<img src='./images/map/pop-td-11.png'>");
            break;
    }
}

function addMarker(coords, map, icon, img) {

    var marker = new google.maps.Marker({
        position:coords,
        icon: {
            url:'data:image/svg+xml;charset=utf-8,' + encodeURIComponent (icon),
            size: new google.maps.Size(200,200),
            scaledSize: new google.maps.Size(32,32),
            anchor: new google.maps.Point(16,16),
            labelOrigin: new google.maps.Point(16,16)
        },
        map: map,
    });

    addInfoWindow(marker, img, map);
}

function addInfoWindow(marker, img, map) {

    var infoWindow = new google.maps.InfoWindow({
        content: img,
        pixelOffset: new google.maps.Size(-85, -1)
    });
    google.maps.event.addListener(marker, 'mouseover', function () {
        infoWindow.open(map, marker);
    });

    google.maps.event.addListener(marker, 'mouseout', function(){
        infoWindow.close();
    });
}

var iconConstruction = '\n' +
    '<svg xmlns="http://www.w3.org/2000/svg" width="50" height="62" fill="none" viewBox="0 0 50 62">\n' +
    '    <path fill="#E0621E" d="M25 24l19 19-19 19L6 43l19-19z"/>\n' +
    '    <path fill="#E0621E" d="M0 0H50V50H0z"/>\n' +
    '    <path fill="#fff" d="M41.534 29.334l-2.303-6.175h-.053c.371-.098.696-.327.915-.645.22-.319.318-.707.28-1.093-.04-.386-.214-.745-.492-1.013-.278-.268-.641-.425-1.025-.445h-.32l-3.481-3.25c.077-.439.019-.891-.168-1.295-.186-.404-.49-.74-.872-.962-.382-.223-.822-.32-1.261-.28-.439.04-.855.216-1.191.504-.336.287-.577.673-.688 1.104-.112.431-.09.887.064 1.305.153.417.43.777.793 1.03.363.252.794.386 1.235.381l.75-.162 3.533 3.304c.042.345.192.668.429.92L30.825 31.5h8.567c1.017 0 2.784-.325 2.142-2.166zM26.005 30.417H11.013c-.71 0-1.39.285-1.893.793-.502.508-.784 1.197-.784 1.915 0 .719.282 1.407.784 1.915s1.183.794 1.893.794h14.992c.71 0 1.39-.286 1.893-.794.502-.508.784-1.196.784-1.915 0-.718-.282-1.407-.784-1.915s-1.183-.793-1.893-.793z"/>\n' +
    '    <path fill="#fff" d="M31.84 20.505c-.945-.254-1.777-.828-2.356-1.625l-1.874 1.787-6.157-6.5h-5.622v6.5H9.406v7.583H26.54l5.3-7.745z"/>\n' +
    '</svg>\n';
var iconWireless = '\n' +
    '<svg xmlns="http://www.w3.org/2000/svg" width="50" height="62" fill="none" viewBox="0 0 50 62">\n' +
    '    <path fill="#E0621E" d="M25 24l19 19-19 19L6 43l19-19z"/>\n' +
    '    <path fill="#E0621E" d="M0 0H50V50H0z"/>\n' +
    '    <path fill="#fff" d="M22.604 18.125c0-.465.129-.92.371-1.312.242-.393.588-.707.998-.906.41-.2.866-.275 1.316-.22.45.056.875.242 1.226.536.351.294.614.684.758 1.125.143.44.162.914.053 1.365-.108.45-.34.861-.666 1.183-.327.322-.737.542-1.18.635v1.032h-.96V20.53c-.54-.114-1.026-.416-1.375-.854s-.54-.986-.54-1.552zm10.063 19.643l-2.1-6.104-4.637 2.858 5.275 3.246h-1.859L25 35.092l-4.346 2.676h-1.86l5.276-3.246-4.638-2.858-2.099 6.104h-6.708v.982h28.75v-.982h-6.708zM29.173 21.41c.712-.94 1.098-2.096 1.098-3.285 0-1.19-.386-2.345-1.098-3.285l-.756.599c.58.769.895 1.714.895 2.686s-.315 1.917-.896 2.686l.758.6zm2.281 1.793c1.097-1.454 1.692-3.24 1.692-5.078s-.595-3.624-1.692-5.078l-.762.6c.97 1.281 1.495 2.857 1.495 4.478 0 1.621-.525 3.197-1.494 4.479l.761.599zm1.514 1.193l.767.604c1.482-1.97 2.286-4.387 2.286-6.875 0-2.488-.804-4.905-2.286-6.875l-.767.604c1.358 1.795 2.094 4 2.094 6.271 0 2.27-.736 4.477-2.093 6.271zm-11.385-3.585c-.58-.769-.895-1.714-.895-2.686s.315-1.917.896-2.686l-.758-.6c-.71.94-1.097 2.097-1.097 3.286 0 1.19.386 2.345 1.098 3.285l.756-.599zm-3.037 2.392l.762-.6c-.97-1.281-1.495-2.857-1.495-4.478 0-1.621.525-3.197 1.495-4.479l-.762-.599c-1.097 1.454-1.692 3.24-1.692 5.078s.595 3.624 1.692 5.078zm-1.514 1.193c-1.358-1.794-2.095-4-2.095-6.271 0-2.27.737-4.476 2.095-6.271l-.767-.604c-1.482 1.97-2.286 4.387-2.286 6.875 0 2.488.804 4.905 2.286 6.875l.767-.604zm10.638-1.139l-.301-.876c-.034-.096-.095-.178-.176-.237-.08-.059-.177-.09-.276-.09h-3.834c-.099 0-.195.031-.276.09-.081.059-.143.141-.176.237l-.302.876L25 25.367l2.67-2.11zm1.996 5.794l-1.66-4.82-2.221 1.755 3.88 3.065zm-5.45-3.065l-2.222-1.754-1.66 4.819 3.881-3.065zm-4.44 4.745L25 33.947l5.224-3.216L25 26.606l-5.224 4.125z"/>\n' +
    '</svg>\n';

var iconEnergy = '\n' +
    '<svg xmlns="http://www.w3.org/2000/svg" width="50" height="62" fill="none" viewBox="0 0 50 62">\n' +
    '    <path fill="#E0621E" d="M25 24l19 19-19 19L6 43l19-19z"/>\n' +
    '    <path fill="#E0621E" d="M0 0H50V50H0z"/>\n' +
    '    <path fill="#fff" d="M22.498 24.976l-.047-1.62c0-.192-.141-.335-.377-.335h-3.915c-.19 0-.33.143-.378.334v1.621h4.717zM40 22.401c0 .191-.142.382-.377.382h-.095l-5.33-1.812c-.142-.047-.236-.19-.236-.334l-.095-1.096c-.188.095-.377.143-.613.143-.188 0-.377-.048-.519-.143l-5.849 2.86c-.047.048-.094.048-.141.048-.142 0-.236-.048-.283-.143-.095-.143-.048-.334.047-.477l4.198-3.766c.094-.095.236-.095.377-.048l.991.477c0-.43.189-.763.519-1.001l.472-6.532c0-.191.141-.334.33-.334.188 0 .33.095.377.286l1.132 5.578c0 .143-.047.287-.141.382l-.897.62c.33.19.567.524.614.953l5.33 3.672c.095 0 .189.143.189.285zM27.787 23.307c0-.19-.142-.333-.33-.333H23.54c-.094 0-.189.047-.236.095-.047.095-.094.19-.094.238l.047 1.622h4.717l-.188-1.622zM17.36 13.009v-1.764c0-.191.14-.382.377-.382.188 0 .377.143.377.382v1.764c0 .19-.141.333-.377.333-.236.048-.378-.095-.378-.333zM18.063 19.922v1.764c0 .19-.141.382-.377.382-.189 0-.377-.143-.377-.382v-1.764c0-.19.141-.381.377-.381.189 0 .377.19.377.381zM20.279 16.489c0 1.43-1.18 2.622-2.595 2.622-1.415 0-2.594-1.192-2.594-2.622s1.18-2.622 2.594-2.622c1.463-.048 2.595 1.144 2.595 2.622zM21.605 19.922c.142.143.142.382 0 .525-.094.095-.141.095-.236.095-.094 0-.188-.048-.236-.095l-1.226-1.24c-.142-.143-.142-.381 0-.524.141-.144.377-.144.519 0l1.18 1.24zM19.856 14.296c-.141-.143-.141-.381 0-.524l1.227-1.24c.141-.143.377-.143.519 0 .141.143.141.381 0 .524l-1.227 1.24c-.094.095-.142.095-.236.095-.141 0-.236-.047-.283-.095zM23.211 16.49c0 .19-.141.381-.377.381h-1.746c-.188 0-.377-.143-.377-.381 0-.191.142-.382.377-.382h1.746c.236 0 .377.19.377.382zM15.52 18.635c.14.143.14.381 0 .524l-1.227 1.24c-.047.095-.142.095-.236.095-.095 0-.189-.047-.236-.095-.141-.143-.141-.382 0-.525l1.226-1.24c.142-.095.33-.095.472 0zM14.29 16.823h-1.745c-.188 0-.377-.143-.377-.381 0-.191.142-.382.377-.382h1.746c.188 0 .377.143.377.382-.047.238-.189.381-.377.381zM13.774 13.056c-.094-.143-.094-.381 0-.524.141-.143.377-.143.519 0l1.226 1.24c.142.143.142.381 0 .524-.047.095-.141.095-.236.095-.094 0-.188-.047-.236-.095l-1.273-1.24zM12.454 23.307c0-.19.141-.333.33-.333H16.7c.095 0 .19.047.236.095.048.095.095.19.095.238v1.67h-4.718l.142-1.67zM22.547 29.172h-4.86l-.046 1.097h4.953l-.047-1.097zM22.503 25.691h-4.765v1.002h4.765V25.69zM22.55 27.407H17.74l-.048 1.05h4.86v-1.05zM22.598 30.984h-4.953l-.047 1.192h5.047l-.047-1.192zM22.637 32.89H17.59l-.047 1.288h5.142l-.047-1.287z"/>\n' +
    '    <path fill="#fff" d="M36.794 38.612h-1.699l-.754-16.878-.378-.143c-.424-.143-.707-.524-.707-.953v-.239c-.142 0-.283 0-.425-.047l-.566.286-.802 17.974h-4.717V37.42h1.887c.189 0 .377-.143.377-.381v-.096l-.236-2.05h-5.141l.094 2.194c0 .19.142.333.33.333h1.934v1.192h-5.519V37.42h1.934c.189 0 .378-.143.378-.381v-.096l-.048-2.097h-5.189l-.047 2.193c0 .095.047.19.095.238.047.048.141.096.236.096h1.934v1.192h-5.472v-1.192h1.934c.188 0 .33-.143.33-.334l.094-2.193H11.51l-.188 2.097v.096c0 .19.141.381.377.381h1.887v1.192h-3.208c-.188 0-.377.143-.377.382 0 .238.142.381.377.381h26.417c.188 0 .377-.143.377-.381 0-.239-.142-.382-.377-.382z"/>\n' +
    '    <path fill="#fff" d="M23.492 32.176h5.048l-.095-1.192h-5l.047 1.192zM23.45 30.269h4.953l-.095-1.097h-4.906l.047 1.097zM23.352 28.456h4.859l-.095-1.049h-4.811l.047 1.05zM28.065 26.693l-.094-1.002h-4.717l.047 1.002h4.764zM23.586 34.178h5.142l-.142-1.287H23.54l.047 1.287zM16.797 30.984h-4.953l-.094 1.192h5l.047-1.192zM16.89 29.172h-4.905l-.094 1.097h4.953l.047-1.097zM16.933 27.407h-4.811l-.095 1.05h4.86l.046-1.05zM17.027 25.691h-4.765l-.094 1.002h4.812l.047-1.002zM16.747 32.89h-5.094l-.094 1.288H16.7l.047-1.287z"/>\n' +
    '</svg>\n';

var iconEngineering = '<svg xmlns="http://www.w3.org/2000/svg" width="50" height="62" fill="none" viewBox="0 0 50 62">\n' +
    '    <path fill="#E0621E" d="M0 0H50V50H0z"/>\n' +
    '    <path fill="#E0621E" d="M25 24l19 19-19 19L6 43l19-19z"/>\n' +
    '    <path fill="#fff" d="M14.136 18.449h12.402c.234 0 .468-.236.468-.472 0-.282-.234-.47-.468-.47h-.655c.14-2.782-1.732-4.903-3.884-5.562v-.849c0-.235-.188-.471-.468-.471h-2.34c-.281 0-.468.236-.468.471v.849c-2.294.707-4.119 2.875-3.885 5.561h-.702c-.234 0-.468.189-.468.471 0 .236.234.472.468.472zM20.364 23.68c2.293 0 4.212-1.839 4.633-4.337h-9.313c.42 2.498 2.386 4.336 4.68 4.336z"/>\n' +
    '    <path fill="#fff" d="M27.082 30.184c-.14-.377-.187-.848.14-1.367 2.809-2.875 1.826-1.885 2.06-2.073.187-.189.375-.33.608-.377-.187-.613-.608-1.132-1.17-1.462-1.403-.8-2.808-1.366-4.212-1.743-.187-.047-.42.047-.514.235l-2.855 5.138h-1.545l-2.808-5.09c-.093-.19-.327-.283-.515-.236-1.404.377-2.808.99-4.212 1.744-.795.424-1.31 1.272-1.357 2.168L10 36.217c0 .141.047.283.14.377.094.094.188.141.328.141h16.614c-.187-.377-.14-.8 0-1.178-.187-.094-.374-.188-.514-.33-.281-.283-.422-.612-.422-.99.094-2.309-.28-3.063.422-3.723.187-.141.327-.236.514-.33z"/>\n' +
    '    <path fill="#fff" d="M39.812 31.222c-.093-.095-.187-.142-.328-.142h-1.357c-.047-.047-.047-.094-.093-.188l.936-.943c.187-.188.187-.471 0-.66l-1.826-1.932c-.187-.189-.468-.189-.655 0l-.936.942c-.047-.047-.093-.047-.187-.047v-1.366c0-.283-.187-.472-.468-.472h-2.715c-.14 0-.234.047-.327.142-.094.094-.14.188-.14.33v1.366c-.047 0-.094.047-.141.047l-.936-.942c-.187-.189-.468-.189-.655 0-.047.094-1.872 1.885-1.92 1.932-.14.283-.186.519.048.707.374.377.655.707.889.896 0 .047-.047.094-.047.188h-1.357c-.14 0-.234.047-.328.142-.093.094-.14.188-.14.33v2.733c0 .142.047.236.14.33.14.141.375.141.515.141h1.17c0 .048.047.095.047.142l-.936.942c-.187.189-.187.472 0 .66l1.919 1.933c.187.188.468.188.655 0l.047-.047.889-.896c.047 0 .094.047.187.094v1.32c0 .236.187.471.468.471h2.715c.14 0 .234-.047.327-.141.094-.094.14-.189.14-.33v-1.367c.047 0 .094-.047.141-.047l.936.943c.094.094.187.141.328.141.14 0 .234-.047.327-.141l1.92-1.933c.186-.188.186-.471 0-.66l-.937-.942c0-.047.047-.094.047-.142h1.357c.281 0 .468-.188.468-.47v-2.734c-.047-.095-.093-.236-.187-.33zm-3.65 1.98c-.094.706-.422 1.366-.983 1.79-.468.377-1.03.566-1.638.566-1.592 0-2.855-1.414-2.668-3.017.187-1.46 1.498-2.545 2.948-2.356 1.498.235 2.528 1.555 2.34 3.016z"/>\n' +
    '</svg>';

var iconTechnologyDeployment = '<svg xmlns="http://www.w3.org/2000/svg" width="50" height="62" fill="none" viewBox="0 0 50 62">\n' +
    '    <path fill="#E0621E" d="M0 0H50V50H0z"/>\n' +
    '    <path fill="#E0621E" d="M25 24l19 19-19 19L6 43l19-19z"/>\n' +
    '    <path fill="#fff" d="M12.113 37.032c0 .228.09.446.252.607.161.162.38.252.608.252h24.062c.228 0 .447-.09.608-.252.161-.16.252-.38.252-.607v-5.156H12.113v5.156zm13.75-3.438h8.594c.228 0 .447.09.608.252.16.161.251.38.251.608 0 .227-.09.446-.251.607-.161.161-.38.252-.608.252h-8.594c-.228 0-.446-.09-.607-.252-.162-.16-.252-.38-.252-.607 0-.228.09-.447.252-.608.16-.161.38-.252.607-.252zm-9.883-.43c.255 0 .505.076.717.218.212.141.377.343.474.578.098.236.123.495.074.745-.05.25-.173.48-.353.66-.18.18-.41.303-.66.353-.25.05-.51.024-.745-.073-.235-.098-.437-.263-.578-.475-.142-.212-.218-.461-.218-.716 0-.342.136-.67.378-.912s.57-.377.911-.377zM35.531 12.97c-.075-.233-.216-.438-.407-.591-.192-.153-.423-.246-.667-.268H15.551c-.25.02-.488.11-.687.263-.198.153-.348.36-.43.596l-1.59 3.438h24.32l-1.633-3.438zM12.113 30.157h25.782V25H12.113v5.157zm13.75-3.438h8.594c.228 0 .447.09.608.252.16.161.251.38.251.608 0 .227-.09.446-.251.607-.161.161-.38.252-.608.252h-8.594c-.228 0-.446-.09-.607-.252-.162-.16-.252-.38-.252-.607 0-.228.09-.447.252-.608.16-.161.38-.252.607-.252zm-9.883-.43c.255 0 .505.076.717.218.212.141.377.343.474.578.098.236.123.495.074.745-.05.25-.173.48-.353.66-.18.18-.41.303-.66.353-.25.05-.51.024-.745-.073-.235-.098-.437-.263-.578-.475-.142-.212-.218-.461-.218-.716 0-.342.136-.67.378-.912s.57-.377.911-.377zm-3.867-8.164v5.157h25.782v-5.157H12.113zm3.867 3.868c-.255 0-.504-.076-.716-.218-.212-.141-.377-.342-.474-.578-.098-.236-.124-.495-.074-.745s.173-.48.353-.66c.18-.18.41-.303.66-.353.25-.05.51-.024.745.074.235.097.437.262.578.474.142.212.218.462.218.717 0 .341-.136.67-.378.911-.242.242-.57.378-.912.378zm18.477-.43h-8.594c-.228 0-.446-.09-.607-.252-.162-.16-.252-.38-.252-.607 0-.228.09-.447.252-.608.16-.161.38-.252.607-.252h8.594c.228 0 .447.09.608.252.16.161.251.38.251.608 0 .227-.09.446-.251.607-.161.161-.38.252-.608.252z"/>\n' +
    '</svg>';

var styles = [
    {
        "featureType": "all",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#000000"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#000000"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#8b9198"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#323336"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#414954"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#2e2f31"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#7a7c80"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#242427"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#202022"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#393a3f"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#202022"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#393a3f"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#202022"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#202124"
            }
        ]
    }
];
