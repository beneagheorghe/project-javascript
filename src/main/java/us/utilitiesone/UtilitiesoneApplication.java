package us.utilitiesone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import us.utilitiesone.handling.StorageProperties;
import us.utilitiesone.services.CachedContentIntf;

@SpringBootApplication
@EntityScan("us.utilitiesone.entities")
@EnableJpaRepositories("us.utilitiesone.repositories")
@EnableJpaAuditing
@EnableCaching
@EnableConfigurationProperties(StorageProperties.class)
public class UtilitiesoneApplication extends SpringBootServletInitializer implements CommandLineRunner {

    @Autowired
    CachedContentIntf cachedContent;

    public static void main(String[] args) {
    	SpringApplication.run(UtilitiesoneApplication.class, args);
    }

    @Override
    public void run(String... args) {
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(UtilitiesoneApplication.class);
    }
}