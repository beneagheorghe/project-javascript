package us.utilitiesone.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import us.utilitiesone.entities.CategoryCareer;

import java.util.Optional;

@Repository
public interface CategoryCareerRepository extends JpaRepository<CategoryCareer, Long> {

    @Query("SELECT c FROM CategoryCareer c JOIN c.vacancies v WHERE c.name = ?1 AND v.active = true")
    Optional<CategoryCareer> findCategoryCareerByName(String name);
}
