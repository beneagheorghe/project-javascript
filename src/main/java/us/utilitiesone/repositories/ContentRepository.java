package us.utilitiesone.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import us.utilitiesone.entities.Content;

import java.util.List;

@Repository
public interface ContentRepository extends JpaRepository<Content, Long> {


}