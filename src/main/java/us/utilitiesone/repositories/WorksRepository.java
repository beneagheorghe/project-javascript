package us.utilitiesone.repositories;

import org.springframework.data.repository.CrudRepository;
import us.utilitiesone.entities.Work;

public interface WorksRepository extends CrudRepository<Work, Long> {
}
