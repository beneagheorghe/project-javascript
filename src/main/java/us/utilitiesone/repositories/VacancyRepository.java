package us.utilitiesone.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import us.utilitiesone.entities.Vacancy;

import javax.persistence.Tuple;
import java.util.List;

@Repository
public interface VacancyRepository extends JpaRepository<Vacancy, Long> {

    @Query("SELECT v FROM Vacancy v WHERE v.categoryCareer.id = ?1")
    List<Vacancy> findVacanciesByCareerId(Long id);

    @Query("SELECT cc.name, COUNT(v) FROM Vacancy v JOIN CategoryCareer cc ON v.categoryCareer.id = cc.id GROUP BY v.categoryCareer.id")
    List<Tuple> findCareerCategoriesWithVacancies();

}
