package us.utilitiesone.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import us.utilitiesone.entities.VacancyRequirements;

import java.util.List;

@Repository
public interface VacancyRequirementsRepository extends JpaRepository<VacancyRequirements, Long> {

    @Query("SELECT vr FROM VacancyRequirements vr WHERE vr.vacancy.id = ?1")
    List<VacancyRequirements> findVacancyRequirementsByVacancyId(Long id);
}
