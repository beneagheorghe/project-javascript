package us.utilitiesone.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import us.utilitiesone.entities.News;

@Repository
public interface NewsRepository extends PagingAndSortingRepository<News, Long> {

    Page<News> findByCategoryNews(String categoryNews, Pageable pageable);
}
