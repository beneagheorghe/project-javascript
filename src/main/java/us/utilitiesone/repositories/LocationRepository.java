package us.utilitiesone.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import us.utilitiesone.entities.Location;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {
}
