package us.utilitiesone.repositories;

import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import us.utilitiesone.entities.Content;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ContentRepositoryCache {

    public final ContentRepository contentRepository;

//    @Cacheable("aliases")
    public List<Content> getAll(String key) {
        return contentRepository.findAll();
    }

//    @CacheEvict("aliases")
    public void update(Content contentObj) {
        contentRepository.save(contentObj);
    }

//    @CacheEvict("aliases")
    public void delete(Content contentObj) {
        contentRepository.delete(contentObj);
    }
}