package us.utilitiesone.entities;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;
import us.utilitiesone.model.MediaModel;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Entity
@Table(name = "news")
@Data
public class News extends MediaModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "short_description")
    private String shortDescription;

    @Column(name = "ctg_news")
    private String categoryNews;

    @Column(name = "link_facebook")
    private String linkFacebook;

    @Column(name = "link_linkedin")
    private String linkLinkedIn;

    @Temporal(TemporalType.DATE)
    @Column(name = "post_date")
    private Date postDate;

    @Transient
    private String monthName;

    public News() {
    }

    public News(@NotBlank(message = "Title is mandatory") String title, String description) {
        this.title = title;
        this.description = description;
    }

    public News(String picName, String picType, byte[] pic, MultipartFile file, @NotBlank(message = "Title is mandatory") String title, String description) {
        super(picName, picType, pic, file);
        this.title = title;
        this.description = description;
    }
}
