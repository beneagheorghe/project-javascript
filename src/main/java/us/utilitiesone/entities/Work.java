package us.utilitiesone.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import us.utilitiesone.model.AuditModel;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@EqualsAndHashCode
@Getter
@Setter
@Entity
@Table(name = "works")
public class Work extends AuditModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotBlank(message = "Title is mandatory")
    @Column(name = "title")
    private String title;

    @Column(name = "alias")
    private String alias;

    @Column(name = "type_of_category_work")
    private String typeOfCategoryWork;

    public Work() {
    }

    public Work(@NotBlank(message = "Title is mandatory") String title, String alias) {
        this.title = title;
        this.alias = alias;
    }
}
