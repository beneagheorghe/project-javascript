package us.utilitiesone.entities;

import lombok.*;
import us.utilitiesone.model.AuditModel;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@EqualsAndHashCode
@Getter
@Setter
@Entity
@Table(name = "content")
public class Content extends AuditModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "content_value")
    private String contentValue;

    @Column(name = "alias")
    private String alias;

    public Content() {
    }

    public Content(@NotBlank(message = "Content value is mandatory") String contentValue, String alias) {
        this.contentValue = contentValue;
        this.alias = alias;
    }
}