package us.utilitiesone.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "vacancy_requirements")
public class VacancyRequirements {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "description")
    String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "vacancy_id")
    private Vacancy vacancy;
}
