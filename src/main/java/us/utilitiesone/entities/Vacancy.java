package us.utilitiesone.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "vacancy")
public class Vacancy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Temporal(TemporalType.DATE)
    @Column(name = "post_date")
    private Date postDate = new Date();

    @Column(name = "title")
    private String title;

    @Column(name = "active")
    private boolean active;

    @Column(name = "description_position")
    private String descriptionPosition;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id")
    private CategoryCareer categoryCareer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id")
    private Location location;

    @OneToMany(mappedBy = "vacancy")
    private List<VacancyRequirements> vacancyRequirements = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescriptionPosition() {
        return descriptionPosition;
    }

    public void setDescriptionPosition(String descriptionPosition) {
        this.descriptionPosition = descriptionPosition;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public CategoryCareer getCategoryCareer() {
        return categoryCareer;
    }

    public void setCategoryCareer(CategoryCareer categoryCareer) {
        this.categoryCareer = categoryCareer;
    }

    public List<VacancyRequirements> getVacancyRequirements() {
        return vacancyRequirements;
    }

    public void setVacancyRequirements(List<VacancyRequirements> vacancyRequirements) {
        this.vacancyRequirements = vacancyRequirements;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
