package us.utilitiesone.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode
@Getter
@Setter
public class Contact {

    private String firstName;

    private String lastName;

    private String email;

    private String subject;

    private String message;

    public Contact() {
    }

    public Contact(String firstName, String lastName, String email, String subject, String message) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.subject = subject;
        this.message = message;
    }
}
