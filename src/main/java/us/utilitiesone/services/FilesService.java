package us.utilitiesone.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import us.utilitiesone.handling.StorageProperties;
import us.utilitiesone.model.MediaModel;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Service
public class FilesService {

    private final Path ROOT_LOCATION;

    @Autowired
    public FilesService(StorageProperties properties) {
        this.ROOT_LOCATION = Paths.get(properties.getLocation());
    }

    @PostConstruct
    public void init() {
        try {
            Files.createDirectories(ROOT_LOCATION);
        } catch (IOException e) {
            e.getStackTrace();
        }
    }

    /**
     * Each entity that will extend {@link MediaModel} can use this method as a helper. In other words,
     * we should have these three fields added to the table that we're going to use:
     * pic(LONGBLOB), pic_name(VARCHAR), pic_type(VARCHAR)
     *
     * @param itemsList - database results for that we are going to generate images
     */
    public void generateImages(Iterable<? extends MediaModel> itemsList) {

        itemsList.forEach(item -> {
            try {
                if (item.getPicName() == null) {
                    return;
                } else {
                    Path filePath = Paths.get(ROOT_LOCATION + File.separator + item.getPicName() + "." + item.getPicType());
                    Files.createDirectories(filePath.getParent());
                    Files.write(filePath, item.getPic());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Build the uploaded image for the model, by extracting all the needed data.
     * As parameter we can input any Object that extends {@link MediaModel}.
     *
     * @param obj - any Object that extends {@link MediaModel}
     * @param <E> - Project, Team or Service, etc.
     * @throws IOException
     */
    public <E extends MediaModel> void buildModelImage(E obj) throws IOException {

        MultipartFile uploadedFile = obj.getFile();

        String fileName = uploadedFile.getOriginalFilename();
        int startIndex = fileName.replaceAll("\\\\", "/").lastIndexOf("/");
        fileName = fileName.substring(startIndex + 1);

        byte[] picArray = uploadedFile.getBytes();
        obj.getFile().getInputStream().read(picArray);

        obj.setPic(picArray);
        obj.setPicName(getRandomUUID());
        obj.setPicType(fileName.substring(fileName.indexOf(".") + 1));
    }

    public void deleteLocalFile(String fileName, String fileType) throws IOException {
        Files.deleteIfExists(Paths.get(ROOT_LOCATION + fileName + "." + fileType));
    }

    public String getRandomUUID() {
        return UUID.randomUUID().toString();
    }
}