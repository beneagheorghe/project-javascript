package us.utilitiesone.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import us.utilitiesone.repositories.ContentRepositoryCache;

import java.util.Optional;

@Slf4j
@Component
@RequiredArgsConstructor
public class CachedContent implements CachedContentIntf {

    private final ContentRepositoryCache contentRepositoryCache;

    public Optional<String> findTitleByAlias(final String alias) {
        return contentRepositoryCache.getAll("aliases")
                .stream()
                .filter(content -> alias.equals(content.getAlias()))
                .map(content -> content.getContentValue())
                .findAny();
    }
}