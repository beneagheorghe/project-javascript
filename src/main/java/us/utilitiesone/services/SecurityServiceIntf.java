package us.utilitiesone.services;

public interface SecurityServiceIntf {

    String findLoggedInUsername();

    void autoLogin(String username, String password);
}
