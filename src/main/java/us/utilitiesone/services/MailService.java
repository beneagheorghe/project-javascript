package us.utilitiesone.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import us.utilitiesone.entities.Contact;
import us.utilitiesone.web.ContractorDto;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Date;

@Slf4j
@RequiredArgsConstructor
@Service
public class MailService {

    @Value("${mail.to}")
    private String mailTo;

    private final JavaMailSender javaMailSender;
    private final MailProperties mailProperties;

    public void buildAndSendContractorsTemplate(ContractorDto contractor, StringBuilder sbWorks) {

        StringBuilder sbContractor = new StringBuilder();
        sbContractor = sbContractor
                .append("Hello U1,").append("<br><br>")
                .append("<b>A new subcontractor has registered:</b>").append("<br><br>")
                .append("Company:").append("<br>")
                .append(contractor.getCompanyName()).append(", ")
                .append(contractor.getCompanyEmail()).append(", ")
                .append(contractor.getCompanyWebsite()).append(", ")
                .append(contractor.getCompanyAddress()).append(", ")
                .append(contractor.getCompanyPhone()).append("<br>")
                .append("You can contact <b>").append(contractor.getCompanyContactName()).append("</b> for any enquiries.").append("<br><br>")
                .append("<b>States:</b>").append("<br>")
                .append("State of incorporation or organization - ").append(contractor.getStateIncorporationOrganization()).append("<br>")
                .append("States in which your company is licensed to work - ").append(contractor.getStateCompanyLicense()).append("<br>")
                .append("Number of employees: ").append(contractor.getNumberOfEmployees()).append("<br>")
                .append("Number of crews available: ").append(contractor.getNumberOfCrews()).append("<br>")
                .append("Type(s) and age of equipment: ").append(contractor.getAgeOfEquipment()).append("<br><br>")
                .append("Worked for Utilities One before? - ").append((contractor.isUtilitiesoneInPast() ? "Yes" : "No")).append("<br>")
                .append("Does the company have the ability to fulfill our insurance requirements? - ").append((contractor.isInsuranceRequirementsFulfill() ? "Yes" : "No")).append("<br>")
                .append("Willing to travel? - ").append((contractor.isWillingToTravel() ? "Yes" : "No")).append("<br><br>")
                .append("<b>Types of work company do:</b>").append("<br>")
                .append(sbWorks).append("<br><br>")
                .append("Experience an prior jobs: ").append(contractor.getExperience()).append("<br>")
                .append("How did you find out about Utilties One? - ").append(contractor.getUtilitiesoneFindOut()).append("<br><br>")
                .append("<b>Comments:</b>").append("<br>")
                .append(contractor.getComments()).append("<br><br>")
                .append("<b>Message:</b>").append("<br>")
                .append(contractor.getMessage());

        sendMail(sbContractor, contractor.getSubject());
    }

    public void buildAndSendContactTemplate(Contact contact) {

        StringBuilder sbContact = new StringBuilder();
        sbContact = sbContact
                .append("Hello U1,").append("<br><br>")
                .append("<b>A new enquiry has been sent from contact-us form:</b>").append("<br><br>")
                .append("<b>Subject:</b>").append("<br>")
                .append(contact.getSubject()).append("<br><br>")
                .append("<b>From:</b>").append("<br>")
                .append(contact.getFirstName()).append(" ").append(contact.getLastName()).append("<br><br>")
                .append("<b>E-mail:</b>").append("<br>")
                .append(contact.getEmail()).append("<br><br>")
                .append("<b>Message:</b>").append("<br>")
                .append(contact.getMessage());

        sendMail(sbContact, contact.getSubject());
    }

    @Async("threadPoolTaskExecutor")
    public void sendMail(StringBuilder sbObj, String subject) {

        try {

            MimeMessage mail = javaMailSender.createMimeMessage();
            mail.addHeader("SpoofingEmailProtection", "5672a3b1-f230-4b10-b823-5853ad58664c");

            MimeMessageHelper helper = new MimeMessageHelper(mail, true);

            helper.setTo(mailTo);
            helper.setFrom(mailProperties.getUsername());

            helper.setSubject(subject);
            helper.setText(sbObj.toString(), true);
            helper.setSentDate(new Date());

            javaMailSender.send(mail);
            log.info("Email form {} to {} was sent", mailProperties.getUsername(), mailTo);

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
