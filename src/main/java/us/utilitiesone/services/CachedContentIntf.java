package us.utilitiesone.services;


import java.util.Optional;

public interface CachedContentIntf {

    Optional<String> findTitleByAlias(final String alias);
}
