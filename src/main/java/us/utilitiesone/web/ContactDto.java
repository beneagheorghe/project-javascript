package us.utilitiesone.web;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class ContactDto {

    @NotEmpty
    private String firstName;

    @NotEmpty
    private String lastName;

    @NotEmpty
    private String email;

    @NotEmpty
    private String subject;

    @NotEmpty
    private String message;
}
