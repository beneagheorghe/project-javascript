package us.utilitiesone.web;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class FormResumeData {

    private String fullName;
    private String email;
    private String phone;
    private MultipartFile multipartFile;
    private String message;
    private String jobTitle;
}
