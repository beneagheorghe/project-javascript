package us.utilitiesone.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ForbiddenController {

    @GetMapping("403")
    public String showAccessDenied() {
        return "403";
    }
}
