package us.utilitiesone.web;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
public class AboutUsController {

    @GetMapping("about-us")
    public String showAboutUs() {
        return "about-us";
    }
}
