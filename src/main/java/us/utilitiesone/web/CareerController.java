package us.utilitiesone.web;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import us.utilitiesone.entities.CategoryCareer;
import us.utilitiesone.entities.Vacancy;
import us.utilitiesone.mail.SMTPMailSender;
import us.utilitiesone.repositories.CategoryCareerRepository;
import us.utilitiesone.repositories.VacancyRepository;

import javax.persistence.Tuple;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
@SessionAttributes("categoryName")
public class CareerController {

    private final CategoryCareerRepository categoryCareerRepository;
    private final SMTPMailSender smtpMailSender;
    private final VacancyRepository vacancyRepository;

    @GetMapping("career")
    public String showCareers() {
        return "career";
    }

    @GetMapping("vacancies")
    public String showCareer(@RequestParam(value = "cn") String categoryName, Model model) {

        List<Tuple> careerCategoriesWithVacancies = vacancyRepository.findCareerCategoriesWithVacancies();
        Optional<CategoryCareer> categoryCareer = categoryCareerRepository.findCategoryCareerByName(categoryName);
        if(!categoryCareer.isPresent()) {
            return "vacancies/" + categoryName.trim().replaceAll(" ", "-");
        }

        model.addAttribute("categoryName", categoryName);
        List<Vacancy> vacancies = categoryCareer.get()
                        .getVacancies()
                        .stream()
                        .filter(vacancy -> vacancy.isActive()).collect(Collectors.toList());
        model.addAttribute("vacancies", vacancies);
        model.addAttribute("categoryCareer", categoryCareer.get());
        model.addAttribute("formResumeData", new FormResumeData());
        model.addAttribute("careerCategoriesWithVacancies", careerCategoriesWithVacancies);

        if ("wireless".equals(categoryName)) {
            return "vacancies/wireless";
        } else if ("construction".equals(categoryName)) {
            return "vacancies/construction";
        } else if ("energy".equals(categoryName)) {
            return "vacancies/energy";
        } else if ("engineering".equals(categoryName)) {
            return "vacancies/engineering";
        } else if ("technology deployment".equals(categoryName)) {
            return "vacancies/technology-deployment";
        } else if ("military and veterans".equals(categoryName)) {
            return "vacancies/military-and-veterans";
        }

        return null;
    }

    @PostMapping("add")
    public String applyResume(FormResumeData formResumeData,
                              @ModelAttribute("categoryName") String categoryCareerParam,
                              RedirectAttributes redirectAttributes) {

        if(formResumeData.getJobTitle().isEmpty()) {
            redirectAttributes.addFlashAttribute("errMsg", "Please select the position above");
            return "redirect:/vacancies?cn=" + categoryCareerParam;
        }

        smtpMailSender.send(formResumeData);

        redirectAttributes.addFlashAttribute("sendSuccessfullyMsg", "Your application was sent successfully");

        return "redirect:/vacancies?cn=" + categoryCareerParam;
    }
}
