package us.utilitiesone.web;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import us.utilitiesone.entities.News;
import us.utilitiesone.repositories.NewsRepository;
import us.utilitiesone.services.FilesService;

import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Controller
@RequiredArgsConstructor
public class NewsFrontController {

    private final FilesService filesService;
    private final NewsRepository newsRepository;

    @GetMapping("news")
    public String showNews(@RequestParam(value = "page") int currentPage, Model model) {

        Page<News> page = newsRepository.findAll(PageRequest.of(currentPage - 1, 5, Sort.by(Sort.Direction.DESC, "postDate")));

        setModelNews(model, page, currentPage);
        filesService.generateImages(page.getContent());

        return  "news";
    }

    @GetMapping("news-ctg")
    public String showNewsIndustry(@RequestParam(value = "page") int currentPage,
                                   @RequestParam(value = "c") String categoryNews,
                                   Model model) {

        Page<News> page = newsRepository.findByCategoryNews(categoryNews, PageRequest.of(currentPage - 1, 5));

        setModelNews(model, page, currentPage);
        model.addAttribute("categoryNews", categoryNews);
        filesService.generateImages(page.getContent());

        return "news-category";
    }

    @GetMapping("news-single")
    public String showAboutUs(@RequestParam(value = "nw_type") long id, Model model, HttpServletResponse response) throws UnsupportedEncodingException {

        Optional<News> news = newsRepository.findById(id);
        news.ifPresent(value -> model.addAttribute("news", value));

        Page<News> latestNews = newsRepository.findAll(PageRequest.of(0, 3, Sort.by(Sort.Direction.ASC, "id")));
        model.addAttribute("listNews", newsList(latestNews));

        String shareUrl = URLEncoder.encode("https://utilitiesone.com/news-single?nw_type=" + id + "&title=" + news.get().getTitle(), StandardCharsets.UTF_8);
        String sharedUtlTwitter = URLEncoder.encode("https://utilitiesone.com/news-single?nw_type=" + id, StandardCharsets.UTF_8);

        model.addAttribute("shareUrl", shareUrl);
        model.addAttribute("sharedUtlTwitter", sharedUtlTwitter);

        return "news-single";
    }

    private void setModelNews(Model model, Page page, int currentPage) {
        model.addAttribute("newsList", newsList(page));
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());
    }

    private List<News> newsList(Page page) {
        List<News> newsList =  page.getContent();
        newsList.forEach(news -> {
            news.setMonthName(Instant.ofEpochMilli(news.getPostDate()
                    .getTime())
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate()
                    .getMonth()
                    .getDisplayName(TextStyle.FULL, Locale.ENGLISH));
        });

        return newsList;
    }
}
