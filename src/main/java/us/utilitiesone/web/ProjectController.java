package us.utilitiesone.web;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
public class ProjectController {

    @GetMapping("projects")
    public String showProjects(Model model) {

        return "projects";
    }
}
