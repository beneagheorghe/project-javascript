package us.utilitiesone.web;

import org.mapstruct.Mapper;
import us.utilitiesone.entities.Work;

import java.util.List;

@Mapper(componentModel = "spring")
public interface WorkMapper {

    List<WorkDto> convertToDtos(List<Work> works);
}
