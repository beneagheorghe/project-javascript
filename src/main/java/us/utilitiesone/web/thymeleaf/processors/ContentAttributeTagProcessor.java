package us.utilitiesone.web.thymeleaf.processors;

import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractAttributeTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.templatemode.TemplateMode;
import us.utilitiesone.services.CachedContentIntf;

public class ContentAttributeTagProcessor extends AbstractAttributeTagProcessor {

    private static final String ATTR_NAME = "content";
    private static final int PRECEDENCE = 10000;

    private CachedContentIntf cachedContentIntf;

    public ContentAttributeTagProcessor(final String dialectPrefix, CachedContentIntf cachedContentIntf) {
        super(
                TemplateMode.HTML,      // This processor will apply only to HTML mode
                dialectPrefix,          // Prefix to be applied to name for matching
                null,       // No tag name: match any tag name
                false,  // No prefix to be applied to tag name
                ATTR_NAME,              // Name of the attribute that will be matched
                true,   // Apply dialect prefix to attribute name
                PRECEDENCE,             // Precedence (inside dialect's precedence)
                true    // Remove the matched attribute afterwards
        );
        this.cachedContentIntf = cachedContentIntf;
    }

    protected void doProcess(
            final ITemplateContext context,
            final IProcessableElementTag tag,
            final AttributeName attributeName,
            final String attributeValue,
            final IElementTagStructureHandler structureHandler
    ) {
        String alias = cachedContentIntf.findTitleByAlias(attributeValue).orElseThrow(() -> new IllegalArgumentException());
        structureHandler.setBody(alias, false);
    }
}