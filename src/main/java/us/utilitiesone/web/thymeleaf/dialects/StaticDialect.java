package us.utilitiesone.web.thymeleaf.dialects;

import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;
import us.utilitiesone.web.thymeleaf.processors.ContentAttributeTagProcessor;
import us.utilitiesone.services.CachedContentIntf;

import java.util.HashSet;
import java.util.Set;

public class StaticDialect extends AbstractProcessorDialect {

    private CachedContentIntf cachedContentIntf;

    public StaticDialect(CachedContentIntf cachedContentIntf) {
        super(
                "Static Dialect",    // Dialect name
                "static",            // Dialect prefix (static:*)
                1000    // Dialect precedence
        );
        this.cachedContentIntf = cachedContentIntf;
    }

    /**
     * Initialize the dialect's processors.
     * <p>
     * Note the dialect prefix is passed here because, although we set
     * "hello" to be the dialect's prefix at the constructor, that only
     * works as a default, and at engine configuration time the user
     * might have chosen a different prefix to be used.
     */
    public Set<IProcessor> getProcessors(final String dialectPrefix) {
        final Set<IProcessor> processors = new HashSet<IProcessor>();
        processors.add(new ContentAttributeTagProcessor(dialectPrefix, cachedContentIntf));
        return processors;
    }
}