package us.utilitiesone.web;

import lombok.Data;

@Data
public class WorkDto {

    private long id;
    private String title;
    private String alias;
    private String typeOfCategoryWork;
}
