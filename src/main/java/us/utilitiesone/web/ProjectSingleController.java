package us.utilitiesone.web;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
public class ProjectSingleController {

    @GetMapping("project-construction")
    public String showProjectConstruction() {
        return "project-construction";
    }

    @GetMapping("project-wireless")
    public String showProjectWireless() {
        return "project-wireless";
    }

    @GetMapping("project-energy")
    public String showProjectEnergy() {
        return "project-energy";
    }

    @GetMapping("project-engineering")
    public String showProjectEngineering() {
        return "project-engineering";
    }

    @GetMapping("project-technology-deployment")
    public String showProjectTechnologyDeployment() {
        return "project-technology-deployment";
    }
}