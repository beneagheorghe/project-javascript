package us.utilitiesone.web;

import lombok.Data;
import us.utilitiesone.entities.Work;

import java.util.List;

@Data
public class ContractorDto {

    private String subject;
    private String companyName;
    private String companyEmail;
    private String companyWebsite;
    private String companyContactName;
    private String companyAddress;
    private String companyPhone;
    private String stateIncorporationOrganization;
    private String stateCompanyLicense;
    private String numberOfEmployees;
    private String numberOfCrews;
    private String ageOfEquipment;
    private boolean utilitiesoneInPast = false;
    private boolean insuranceRequirementsFulfill = false;
    private boolean willingToTravel = false;
    private String experience;
    private String utilitiesoneFindOut;
    private String comments;
    private String message;
    private boolean consent = false;

    private List<Work> works;
}
