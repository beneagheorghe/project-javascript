package us.utilitiesone.web;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import us.utilitiesone.entities.Contact;
import us.utilitiesone.services.MailService;
import javax.validation.Valid;


@Controller
@RequiredArgsConstructor
public class ContactUsController {

    private final MailService mailService;
    private final ContactMapper contactMapper;

    @GetMapping("contact-us")
    public String showContactUs(Model model) {

        model.addAttribute("contact", new ContactDto());

        return "contact-us";
    }

    @PostMapping("contact-us/send")
    public String sendContactUs(@Valid ContactDto contactDto, BindingResult result, RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("message", "error");
            return "redirect:/contact-us";
        }

        Contact contact = contactMapper.convertToEntity(contactDto);
        mailService.buildAndSendContactTemplate(contact);

        redirectAttributes.addFlashAttribute("message", "success");
        return "redirect:/contact-us";
    }

}
