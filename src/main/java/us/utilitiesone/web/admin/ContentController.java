package us.utilitiesone.web.admin;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import us.utilitiesone.entities.Content;
import us.utilitiesone.repositories.ContentRepository;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
@RequestMapping("admin/content")
public class ContentController {

    private final ContentRepository contentRepository;

    @GetMapping({"", "/"})
    public String show(Model model) {

        model.addAttribute("content", new Content());
        model.addAttribute("contentsList", contentRepository.findAll());

        return "admin/content";
    }

    @PostMapping("add")
    public String add(
            @Valid Content content,
            BindingResult result,
            Model model) {

        if (result.hasErrors()) {
            return "redirect:/admin/content";
        }

        contentRepository.save(content);

        return "redirect:/admin/content";
    }

    @GetMapping("edit/{id}")
    public String edit(
            @PathVariable("id") long id,
            Model model
    ) {

        Content content = contentRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid content id: " + id));
        model.addAttribute("content", content);

        return "admin/content/edit";
    }

    @PostMapping("update/{id}")
    public String update(
            @PathVariable("id") int id,
            @Valid Content content,
            BindingResult result,
            Model model
    ) {

        if (result.hasErrors()) {
            content.setId(id);
            return "redirect:/admin/content/edit/" + id;
        }

        contentRepository.save(content);

        model.addAttribute("contentList", contentRepository.findAll());

        return "redirect:/admin/content/edit/" + id;
    }

    @GetMapping("delete/{id}")
    public String delete(
            @PathVariable("id") long id,
            Model model
    ) {

        Content content = contentRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid content id: " + id));
        contentRepository.delete(content);
        model.addAttribute("contentList", contentRepository.findAll());

        return "redirect:/admin/content";
    }
}
