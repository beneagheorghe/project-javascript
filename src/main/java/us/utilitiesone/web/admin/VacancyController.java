package us.utilitiesone.web.admin;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import us.utilitiesone.entities.CategoryCareer;
import us.utilitiesone.entities.Vacancy;
import us.utilitiesone.repositories.CategoryCareerRepository;
import us.utilitiesone.repositories.LocationRepository;
import us.utilitiesone.repositories.VacancyRepository;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("admin/vacancy")
@SessionAttributes("categoryCareer")
public class VacancyController {

    private final VacancyRepository vacancyRepository;
    private final CategoryCareerRepository categoryCareerRepository;
    private final LocationRepository locationRepository;

    @GetMapping("{id}")
    public String show(@PathVariable("id") long id, Model model) {

        CategoryCareer categoryCareer = categoryCareerRepository
                .findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid category career id: " + id));

        List<Vacancy> vacancies = vacancyRepository.findVacanciesByCareerId(id);

        model.addAttribute("categoryCareer", categoryCareer);
        model.addAttribute("vacancy", new Vacancy());
        model.addAttribute("vacancies", vacancies);
        model.addAttribute("locations", locationRepository.findAll());

        return "admin/vacancy";
    }

    @PostMapping("add")
    public String add(@Valid Vacancy vacancy, BindingResult result, @ModelAttribute("categoryCareer") CategoryCareer categoryCareer) {

        if (result.hasErrors()) {
            return "redirect:/admin/vacancy";
        }
        vacancy.setCategoryCareer(categoryCareer);
        vacancyRepository.save(vacancy);

        return "redirect:/admin/vacancy/" + categoryCareer.getId();
    }

    @GetMapping("edit/{id}")
    public String edit(@PathVariable("id") long id, Model model) {
        Vacancy vacancy = vacancyRepository
                .findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid vacancy id: " + id));
        model.addAttribute("vacancy", vacancy);
        model.addAttribute("locations", locationRepository.findAll());

        return "admin/vacancy/edit";
    }

    @PostMapping("update/{id}")
    public String update(@PathVariable("id") long id, @Valid Vacancy vacancy, BindingResult result) {

        if (result.hasErrors()) {
            vacancy.setId(id);
            return "redirect:/admin/vacancy/edit";
        }
        Vacancy v = vacancyRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid category career id: " + id));
        vacancy.setCategoryCareer(v.getCategoryCareer());
        vacancyRepository.save(vacancy);

        return "redirect:/admin/vacancy/" + v.getCategoryCareer().getId();
    }
}
