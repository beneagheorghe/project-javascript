package us.utilitiesone.web.admin;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import us.utilitiesone.entities.News;
import us.utilitiesone.repositories.LocationRepository;
import us.utilitiesone.repositories.NewsRepository;
import us.utilitiesone.services.FilesService;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("admin/news")
public class NewsController {

    private final FilesService filesService;
    private final NewsRepository newsRepository;

    @GetMapping({"", "/"})
    public String show(Model model) {
        return showPageable(1, model);
    }

    @GetMapping("/{page}")
    public String showPageable(@PathVariable(value = "page") int currentPage, Model model) {

        Pageable pageable = PageRequest.of(currentPage - 1, 5);
        Page<News> page = newsRepository.findAll(pageable);

        List<News> news = page.getContent();

        model.addAttribute("news", new News());
        model.addAttribute("newsList", news);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());

        filesService.generateImages(news);

        return  "admin/news";
    }

    @PostMapping("add")
    public String add(@Valid News news, BindingResult result) throws IOException {

        if (result.hasErrors()) {
            return "redirect:/admin/news";
        }

        filesService.buildModelImage(news);

        newsRepository.save(news);

        return "redirect:/admin/news";
    }

    @GetMapping("edit/{id}")
    public String edit(@PathVariable("id") long id, Model model) {
        News news = newsRepository
                .findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid news id: " + id));
        model.addAttribute("news", news);

        return "admin/news/edit";
    }

    @PostMapping("update/{id}")
    public String update(@PathVariable("id") long id, @Valid News news, BindingResult result) throws IOException {

        if (result.hasErrors()) {
            news.setId(id);
            return "redirect:/admin/news/edit";
        }

        if (news.getFile().getSize() > 0) {
            filesService.deleteLocalFile(news.getPicName(), news.getPicType());
            filesService.buildModelImage(news);
        } else {
            News newsObj = newsRepository.findById(id).get();

            news.setPic(newsObj.getPic());
            news.setPicName(newsObj.getPicName());
            news.setPicType(newsObj.getPicType());
        }

        newsRepository.save(news);

        return "redirect:/admin/news";
    }

    @GetMapping("delete/{id}")
    public String delete(@PathVariable("id") long id) {
        News news = newsRepository
                .findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid news id: " + id));
        newsRepository.delete(news);

        return "redirect:/admin/news";
    }
}
