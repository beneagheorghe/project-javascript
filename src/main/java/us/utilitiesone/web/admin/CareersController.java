package us.utilitiesone.web.admin;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import us.utilitiesone.entities.CategoryCareer;
import us.utilitiesone.repositories.CategoryCareerRepository;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("admin/careers")
public class CareersController {

    private final CategoryCareerRepository categoryCareerRepository;

    @GetMapping({"", "/"})
    public String show(Model model) {

        List<CategoryCareer> categoryCareers = categoryCareerRepository.findAll();

        model.addAttribute("categoryCareer", new CategoryCareer());
        model.addAttribute("categoryCareers", categoryCareers);

        return "admin/careers";
    }

    @PostMapping("add")
    public String add(@Valid CategoryCareer categoryCareer, BindingResult result) {

        if (result.hasErrors()) {
            return "redirect:/admin/careers";
        }

        categoryCareerRepository.save(categoryCareer);

        return "redirect:/admin/careers";
    }

    @GetMapping("edit/{id}")
    public String edit(@PathVariable("id") long id, Model model) {

        CategoryCareer categoryCareer = categoryCareerRepository
                .findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid service id: " + id));
        model.addAttribute("categoryCareer", categoryCareer);

        return "admin/careers/edit";
    }

    @PostMapping("update/{id}")
    public String update(@PathVariable("id") long id, @Valid CategoryCareer categoryCareer, BindingResult result) {

        if (result.hasErrors()) {
            categoryCareer.setId(id);
            return "redirect:/admin/careers/edit";
        }

        categoryCareerRepository.save(categoryCareer);

        return "redirect:/admin/careers";
    }
}
