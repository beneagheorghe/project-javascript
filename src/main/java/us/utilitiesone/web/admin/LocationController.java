package us.utilitiesone.web.admin;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import us.utilitiesone.entities.Location;
import us.utilitiesone.entities.News;
import us.utilitiesone.repositories.LocationRepository;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("admin/locations")
public class LocationController {

    private final LocationRepository locationRepository;

    @GetMapping({"", "/"})
    public String show(Model model) {

        List<Location> locations = locationRepository.findAll();

        model.addAttribute("location", new Location());
        model.addAttribute("locations", locations);

        return "admin/locations";
    }

    @PostMapping("add")
    public String add(@Valid Location location, BindingResult result) throws IOException {

        if (result.hasErrors()) {
            return "redirect:/admin/locations";
        }

        locationRepository.save(location);

        return "redirect:/admin/locations";
    }

    @GetMapping("delete/{id}")
    public String delete(@PathVariable("id") long id) {
        Location location = locationRepository
                .findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid location id: " + id));
        locationRepository.delete(location);

        return "redirect:/admin/locations";
    }
}
