package us.utilitiesone.web.admin;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import us.utilitiesone.entities.Vacancy;
import us.utilitiesone.entities.VacancyRequirements;
import us.utilitiesone.repositories.VacancyRepository;
import us.utilitiesone.repositories.VacancyRequirementsRepository;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("admin/vacancy-requirements")
@SessionAttributes("vacancy")
public class VacancyRequirementsController {

    private final VacancyRepository vacancyRepository;
    private final VacancyRequirementsRepository vacancyRequirementsRepository;

    @GetMapping("{id}")
    public String show(@PathVariable("id") long id, Model model) {
        Vacancy vacancy = vacancyRepository
                .findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid category career id: " + id));

        List<VacancyRequirements> vacancyRequirementsList = vacancyRequirementsRepository.findVacancyRequirementsByVacancyId(id);

        model.addAttribute("vacancy", vacancy);
        model.addAttribute("vacancyRequirements", new VacancyRequirements());
        model.addAttribute("vacancyRequirementsList", vacancyRequirementsList);

        return "admin/vacancy-requirements";
    }

    @PostMapping("add")
    public String add(@Valid VacancyRequirements vacancyRequirements,
                      BindingResult result,
                      @ModelAttribute("vacancy") Vacancy vacancy) {

        if (result.hasErrors()) {
            return "redirect:/admin/vacancy-requirements";
        }
        vacancyRequirements.setVacancy(vacancy);
        vacancyRequirementsRepository.save(vacancyRequirements);

        return "redirect:/admin/vacancy-requirements/" + vacancy.getId();
    }

    @GetMapping("edit/{id}")
    public String edit(@PathVariable("id") long id, Model model) {
        VacancyRequirements vacancyRequirements = vacancyRequirementsRepository
                .findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid vacancy requirements id: " + id));
        model.addAttribute("vacancyRequirements", vacancyRequirements);

        return "admin/vacancyRequirements/edit";
    }

    @GetMapping("delete/{id}")
    public String delete(@PathVariable("id") long id, Model model) {
        VacancyRequirements vacancyRequirements = vacancyRequirementsRepository
                .findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid service id: " + id));
        vacancyRequirementsRepository.delete(vacancyRequirements);

        return "redirect:/admin/vacancy-requirements/" + vacancyRequirements.getVacancy().getId();
    }
}
