package us.utilitiesone.web;

import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import us.utilitiesone.entities.Work;
import us.utilitiesone.repositories.WorksRepository;
import us.utilitiesone.services.MailService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class SubContractorController {

    private final WorksRepository worksRepository;
    private final MailService mailService;
    private final WorkMapper workMapper;

    @GetMapping("subcontractors")
    public String showSubcontractors(Model model) {

        model.addAttribute("contractor", new ContractorDto());

        Iterable<Work> worksList = worksRepository.findAll();
        List<WorkDto> workDtos = workMapper.convertToDtos(Lists.newArrayList(worksList));
        model.addAttribute("worksList", workDtos);

        return "subcontractors";
    }

    @PostMapping("add-subcontractor")
    public String addSubcontractors(@Valid ContractorDto contractorDto,
                                    @RequestParam(value = "works", required = false) long[] works,
                                    BindingResult result) {

        if (result.hasErrors()) {
            return "redirect:/subcontractors";
        }

        List<Work> worksList = new ArrayList<>();
        StringBuilder sbWorks = new StringBuilder();
        int i = 0;

        if (works != null) {

            for (long workId : works) {
                Work work = worksRepository.findById(workId).get();
                worksList.add(work);
                sbWorks.append(work.getTitle()).append(i++ == works.length - 1 ? "" : ", "); // <- check for last iteration
            }

            contractorDto.setWorks(worksList);
        }

        mailService.buildAndSendContractorsTemplate(contractorDto, sbWorks);

        return "redirect:/subcontractors";
    }
}
