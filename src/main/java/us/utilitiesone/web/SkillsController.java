package us.utilitiesone.web;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequiredArgsConstructor
public class SkillsController {

    @GetMapping("skills")
    public String showConstruction(@RequestParam(value = "s") String skill) {

        if ("construction".equals(skill)) {
            return "skills/construction";
        } else if ("wireless".equals(skill)) {
            return "skills/wireless";
        } else if ("energy".equals(skill)) {
            return "skills/energy";
        } else if ("engineering".equals(skill)) {
            return "skills/engineering";
        } else if ("technology-deployment".equals(skill)) {
            return "skills/technology-deployment";
        }

        return null;

    }
}
