package us.utilitiesone.web;

import org.mapstruct.Mapper;
import us.utilitiesone.entities.Contact;

@Mapper(componentModel = "spring")
public interface ContactMapper {

    Contact convertToEntity(ContactDto contactDto);
}
