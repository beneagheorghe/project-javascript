package us.utilitiesone.model;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;

/**
 * Here you can keep common columns from entities
 */
@MappedSuperclass
@EqualsAndHashCode
@Getter
@Setter
public abstract class MediaModel extends AuditModel {

    @Column(name = "pic_name")
    protected String picName;

    @Column(name = "pic_type")
    protected String picType;

    @Lob
    @Column(name = "pic")
    protected byte[] pic;

    @Transient
    protected MultipartFile file;

    public MediaModel() {
    }

    public MediaModel(String picName, String picType, byte[] pic, MultipartFile file) {
        this.picName = picName;
        this.picType = picType;
        this.pic = pic;
        this.file = file;
    }
}
