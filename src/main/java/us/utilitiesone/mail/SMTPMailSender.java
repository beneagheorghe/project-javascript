package us.utilitiesone.mail;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import us.utilitiesone.web.FormResumeData;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;

@Slf4j
@RequiredArgsConstructor
@Component
public class SMTPMailSender {

    @Value("${mail.to}")
    private String mailTo;

    private final JavaMailSender javaMailSender;
    private final MailProperties mailProperties;
    private final Configuration freeMarker;

    public boolean send(FormResumeData formData) {

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        try {
            freeMarker.setClassForTemplateLoading(this.getClass(), "/templates/mail/");
            Template template = freeMarker.getTemplate("email-template.ftl");
            String htmlContent = FreeMarkerTemplateUtils.processTemplateIntoString(template, formData);

            mimeMessage.addHeader("SpoofingEmailProtection", "5672a3b1-f230-4b10-b823-5853ad58664c");

            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);

            mimeMessageHelper.setTo(InternetAddress.parse(mailTo));
            mimeMessageHelper.setSubject("Application Post for UtilitiesOne Team");
            mimeMessageHelper.setFrom(mailProperties.getUsername());
            mimeMessageHelper.setText("", htmlContent);

            ByteArrayDataSource attachmentStream = new ByteArrayDataSource(formData.getMultipartFile().getInputStream(),
                    formData.getMultipartFile().getContentType());
            mimeMessageHelper.addAttachment(formData.getMultipartFile().getOriginalFilename(), attachmentStream);

            javaMailSender.send(mimeMessage);

            log.info("Message was send from {} to {} subject {} data {}", mailProperties.getUsername(), mailTo, mimeMessage.getSubject(), formData);
        } catch (MailException | IOException | TemplateException | MessagingException e) {
            log.error("Mail cannot be send", e);
            return false;
        }
        return true;
    }
}
