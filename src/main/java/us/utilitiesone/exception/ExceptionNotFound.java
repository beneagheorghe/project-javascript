package us.utilitiesone.exception;

import org.springframework.web.bind.annotation.ExceptionHandler;

public class ExceptionNotFound {

    @ExceptionHandler(IllegalArgumentException.class)
    private  String handleAllException(Exception ex) {
        return "/error";
    }
}
