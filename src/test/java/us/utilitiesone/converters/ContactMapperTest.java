package us.utilitiesone.converters;

import org.junit.Test;
import org.mapstruct.factory.Mappers;
import us.utilitiesone.entities.Contact;
import us.utilitiesone.web.ContactDto;
import us.utilitiesone.web.ContactMapper;

import static org.junit.Assert.assertEquals;

public class ContactMapperTest {

    private ContactMapper mapper = Mappers.getMapper(ContactMapper.class);

    @Test
    public void givenSourceToDestination() {

        ContactDto contactDto = new ContactDto();
        contactDto.setFirstName("John");
        contactDto.setLastName("Dallas");
        contactDto.setMessage("some message");
        contactDto.setEmail("test@mail.com");
        contactDto.setSubject("some subject");

        Contact contact = mapper.convertToEntity(contactDto);

        assertEquals(contactDto.getFirstName(), contact.getFirstName());
        assertEquals(contactDto.getLastName(), contact.getLastName());
        assertEquals(contactDto.getEmail(), contact.getEmail());
        assertEquals(contactDto.getMessage(), contact.getMessage());
        assertEquals(contactDto.getSubject(), contact.getSubject());
    }
}
