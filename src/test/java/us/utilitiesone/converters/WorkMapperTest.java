package us.utilitiesone.converters;

import org.junit.Test;
import org.mapstruct.factory.Mappers;
import us.utilitiesone.entities.Work;
import us.utilitiesone.web.WorkMapper;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class WorkMapperTest {

    private WorkMapper mapper = Mappers.getMapper(WorkMapper.class);

    @Test
    public void givenSourceToDestination() {

        Work work = new Work();
        work.setId(12345);
        work.setTitle("some title");
        work.setAlias("some alias");
        mapper.convertToDtos(Arrays.asList(work)).forEach(workDto -> {
            assertEquals(workDto.getId(), work.getId());
            assertEquals(workDto.getTitle(), work.getTitle());
            assertEquals(workDto.getAlias(), work.getAlias());
        });
    }

}
