package us.utilitiesone.controllers.admin;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import us.utilitiesone.UtilitiesoneApplication;
import us.utilitiesone.controllers.ControllersTestEnvironment;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = {UtilitiesoneApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ContentControllerTest extends ControllersTestEnvironment {

    @Test
    public void givenPathOnAdminContentNoAuth_shouldSucceedWith401() {
        // given
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/admin/content"), HttpMethod.GET, entity, String.class);

        // when + then
        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
    }

//    @Test
//    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
//    public void givenPathOnAdminContentAuthenticated_shouldSucceedWith401() {
//        // given
//        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
//        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/admin/content"), HttpMethod.GET, entity, String.class);
//
//        // when + then
//        assertEquals(HttpStatus.OK, response.getStatusCode());
//    }
}