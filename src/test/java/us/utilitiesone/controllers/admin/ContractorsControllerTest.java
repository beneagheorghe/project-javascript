package us.utilitiesone.controllers.admin;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import us.utilitiesone.UtilitiesoneApplication;
import us.utilitiesone.controllers.ControllersTestEnvironment;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = {UtilitiesoneApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ContractorsControllerTest extends ControllersTestEnvironment {

    @Test
    public void givenPathOnAdminContractorsNoAuth_shouldSucceedWith401() {
        // given
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/admin/subcontractors"), HttpMethod.GET, entity, String.class);

        // when + then
        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
    }
}
