package us.utilitiesone.controllers.admin;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import us.utilitiesone.UtilitiesoneApplication;
import us.utilitiesone.controllers.ControllersTestEnvironment;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = {UtilitiesoneApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AdminControllerTest extends ControllersTestEnvironment {

    @Autowired
    ApplicationContext context;

    @Autowired
    AuthenticationManager manager;

    @Test
    public void givenPathOnAdminNoAuth_shouldErrorWith401() {
        // given
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/admin"), HttpMethod.GET, entity, String.class);

        // when + then
        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
    }

//    @Test
//    public void givenAuthentication_whenAccessHome_thenOK() {
//
//        headers.add("Authorization", "Basic YWRtaW46YWRtaW4=");
//
//        // given
//        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
//        ResponseEntity<String> response = restTemplate.withBasicAuth("admin", "admin").getForEntity(createURLWithPort("/admin"), String.class);
////        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/admin"), HttpMethod.GET, entity, String.class);
//
//        // when + then
//        assertEquals(HttpStatus.OK, response.getStatusCode());
//    }
}