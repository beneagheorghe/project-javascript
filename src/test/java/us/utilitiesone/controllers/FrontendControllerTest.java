package us.utilitiesone.controllers;


import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import us.utilitiesone.UtilitiesoneApplication;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = {UtilitiesoneApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FrontendControllerTest extends ControllersTestEnvironment {

    @Test
    public void givenPathOnHome_shouldSucceedWith200() {
        // given
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> responseRoot = restTemplate.exchange(createURLWithPort(""), HttpMethod.GET, entity, String.class);
        ResponseEntity<String> responseRootSecond = restTemplate.exchange(createURLWithPort("/"), HttpMethod.GET, entity, String.class);
        ResponseEntity<String> responseHome = restTemplate.exchange(createURLWithPort("/home"), HttpMethod.GET, entity, String.class);

        // when + then
        assertEquals(HttpStatus.OK, responseRoot.getStatusCode());
        assertEquals(HttpStatus.OK, responseRootSecond.getStatusCode());
        assertEquals(HttpStatus.OK, responseHome.getStatusCode());
    }

    @Test
    public void givenPathAndWrongHttpMethodOnHome_shouldSucceedWithMethodNotAllowed() {
        // given
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> responseRootPost = restTemplate.exchange(createURLWithPort(""), HttpMethod.POST, entity, String.class);
        ResponseEntity<String> responseRootPut = restTemplate.exchange(createURLWithPort(""), HttpMethod.PUT, entity, String.class);
        ResponseEntity<String> responseRootDelete = restTemplate.exchange(createURLWithPort(""), HttpMethod.DELETE, entity, String.class);
        ResponseEntity<String> responseRootSlashPost = restTemplate.exchange(createURLWithPort("/"), HttpMethod.POST, entity, String.class);
        ResponseEntity<String> responseRootSlashPut = restTemplate.exchange(createURLWithPort("/"), HttpMethod.PUT, entity, String.class);
        ResponseEntity<String> responseRootSlashDelete = restTemplate.exchange(createURLWithPort("/"), HttpMethod.DELETE, entity, String.class);
        ResponseEntity<String> responseHomePost = restTemplate.exchange(createURLWithPort("/home"), HttpMethod.POST, entity, String.class);
        ResponseEntity<String> responseHomePut = restTemplate.exchange(createURLWithPort("/home"), HttpMethod.PUT, entity, String.class);
        ResponseEntity<String> responseHomeDelete = restTemplate.exchange(createURLWithPort("/home"), HttpMethod.DELETE, entity, String.class);

        // when + then
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responseRootPost.getStatusCode());
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responseRootPut.getStatusCode());
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responseRootDelete.getStatusCode());
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responseRootSlashPost.getStatusCode());
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responseRootSlashPut.getStatusCode());
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responseRootSlashDelete.getStatusCode());
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responseHomePost.getStatusCode());
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responseHomePut.getStatusCode());
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responseHomeDelete.getStatusCode());
    }

    @Test
    public void givenPathOnAboutUs_shouldSucceedWith200() {
        // given
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/about-us"), HttpMethod.GET, entity, String.class);

        // when + then
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void givenPathOnAboutUs_shouldSucceedWithMethodNotAllowed() {
        // given
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> responsePost = restTemplate.exchange(createURLWithPort("/about-us"), HttpMethod.POST, entity, String.class);
        ResponseEntity<String> responsePut = restTemplate.exchange(createURLWithPort("/about-us"), HttpMethod.PUT, entity, String.class);
        ResponseEntity<String> responseDelete = restTemplate.exchange(createURLWithPort("/about-us"), HttpMethod.DELETE, entity, String.class);

        // when + then
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responsePost.getStatusCode());
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responsePut.getStatusCode());
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responseDelete.getStatusCode());
    }

   @Test
    public void givenPathOnServices_shouldSucceedWith200() {
        // given
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/services"), HttpMethod.GET, entity, String.class);

        // when + then
        //assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void givenPathOnServices_shouldSucceedWithNoMethodAllowed() {
        // given
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> responsePost = restTemplate.exchange(createURLWithPort("/services"), HttpMethod.POST, entity, String.class);
        ResponseEntity<String> responsePut = restTemplate.exchange(createURLWithPort("/services"), HttpMethod.PUT, entity, String.class);
        ResponseEntity<String> responseDelete = restTemplate.exchange(createURLWithPort("/services"), HttpMethod.DELETE, entity, String.class);

        // when + then
       // assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responsePost.getStatusCode());
        //assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responsePut.getStatusCode());
       // assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responseDelete.getStatusCode());
    }

    @Test
    public void givenPathOnProjects_shouldSucceedWith200() {
        // given
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/projects"), HttpMethod.GET, entity, String.class);

        // when + then
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void givenPathOnProjects_shouldSucceedWithMethodNotAllowed() {
        // given
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> responsePost = restTemplate.exchange(createURLWithPort("/projects"), HttpMethod.POST, entity, String.class);
        ResponseEntity<String> responsePut = restTemplate.exchange(createURLWithPort("/projects"), HttpMethod.PUT, entity, String.class);
        ResponseEntity<String> responseDelete = restTemplate.exchange(createURLWithPort("/projects"), HttpMethod.DELETE, entity, String.class);

        // when + then
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responsePost.getStatusCode());
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responsePut.getStatusCode());
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responseDelete.getStatusCode());
    }



    @Test
    public void givenPathOnSubcontractors_shouldSucceedWith200() {
        // given
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/subcontractors"), HttpMethod.GET, entity, String.class);

        // when + then
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void givenPathOnSubcontractors_shouldSucceedWithMethodNotAllowed() {
        // given
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> responsePost = restTemplate.exchange(createURLWithPort("/subcontractors"), HttpMethod.POST, entity, String.class);
        ResponseEntity<String> responsePut = restTemplate.exchange(createURLWithPort("/subcontractors"), HttpMethod.PUT, entity, String.class);
        ResponseEntity<String> responseDelete = restTemplate.exchange(createURLWithPort("/subcontractors"), HttpMethod.DELETE, entity, String.class);

        // when + then
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responsePost.getStatusCode());
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responsePut.getStatusCode());
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responseDelete.getStatusCode());
    }

    @Test
    public void givenPathOnContactUs_shouldSucceedWith200() {
        // given
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/contact-us"), HttpMethod.GET, entity, String.class);

        // when + then
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void givenPathOnContactUs_shouldSucceedWithMethodNotAllowed() {
        // given
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> responsePost = restTemplate.exchange(createURLWithPort("/contact-us"), HttpMethod.POST, entity, String.class);
        ResponseEntity<String> responsePut = restTemplate.exchange(createURLWithPort("/contact-us"), HttpMethod.PUT, entity, String.class);
        ResponseEntity<String> responseDelete = restTemplate.exchange(createURLWithPort("/contact-us"), HttpMethod.DELETE, entity, String.class);

        // when + then
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responsePost.getStatusCode());
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responsePut.getStatusCode());
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responseDelete.getStatusCode());
    }

    @Test
    public void givenPathOnCareer_shouldSucceedWith200() {
        // given
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/career"), HttpMethod.GET, entity, String.class);

        // when + then
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void givenPathOnCareer_shouldSucceedWithMethodNotAllowed() {
        // given
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> responsePost = restTemplate.exchange(createURLWithPort("/career"), HttpMethod.POST, entity, String.class);
        ResponseEntity<String> responsePut = restTemplate.exchange(createURLWithPort("/career"), HttpMethod.PUT, entity, String.class);
        ResponseEntity<String> responseDelete = restTemplate.exchange(createURLWithPort("/career"), HttpMethod.DELETE, entity, String.class);

        // when + then
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responsePost.getStatusCode());
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responsePut.getStatusCode());
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, responseDelete.getStatusCode());
    }
}