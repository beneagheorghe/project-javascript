'use strict';
const gulp = require('gulp');
const sass = require('gulp-sass');

gulp.task('scss', function (done) {
    return gulp.src('src/main/resources/static/scss/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('src/main/resources/static/css'));
});

gulp.task('watch', function () {
    gulp.watch('src/main/resources/static/scss/**/*.scss', gulp.series('scss'));
});
