CREATE TABLE news (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  created_at datetime(6) NOT NULL DEFAULT '1970-01-02',
  updated_at datetime(6) NOT NULL DEFAULT '1970-01-02',
  title varchar(255) DEFAULT NULL,
  pic longblob,
  pic_name varchar(255) DEFAULT NULL,
  pic_type varchar(255) DEFAULT NULL,
  description text,
  post_date date NOT NULL,
  short_description text NOT NULL,
  PRIMARY KEY (`id`)
);

alter table news modify column created_at datetime(6) not null;
alter table news modify column updated_at datetime(6) not null;
alter table news add column ctg_news char(2) not null;
alter table news add column link_facebook text;
alter table news add column link_linkedin text;