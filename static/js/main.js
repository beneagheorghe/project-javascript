$(document).ready(function() {
// var mapsssss =
// console.log(mapsssss)
//     $('title').text("Engineering, Construction & Energy Solutions | Utilities One")
    console.log(window.location.pathname)

    if(window.location.pathname === "/"){
        document.title = "Engineering, Construction & Energy Solutions | Utilities One"
    }
    if(window.location.pathname === "/about-us"){
        document.title = "Find out who we are and how we operate | Utilities One"
    }
    if(window.location.pathname === "/news"){
        document.title = "Read the latest industry and company news | Utilities One"
    }
    if(window.location.pathname === "/expertise"){
        document.title = "Exceptional expertise in numerous domains | Utilities One"
    }
    if(window.location.pathname === "/contact-us"){
        document.title = "Contact Us directly or send us a message | Utilities One"
    }
    if(window.location.pathname === "/career"){
        document.title = "Job hunting? Build your career with us | Utilities One"
    }
    if(window.location.pathname === "/projects"){
        document.title = "Our latest projects and work examples | Utilities One"
    }

    $('.uk-panel').on('click tap', function(){
        $('.gm-style').find('div').find('div').find('div').find('div').find('div').addClass('norok')
        if($('.gm-style').find('div').find('div').find('div').find('div').find('div').hasClass('norok')){
            alert('eeee')
        }
    })
    let ourPartnersSlider = $('.ourPartnersSlider > .owl-carousel');
    ourPartnersSlider.owlCarousel({
        items:5,
        loop:true,
        margin:10,
        autoplay:true,
        autoplayTimeout:2000,
        autoplayHoverPause:true,
        responsive:{
            0:{
                items:2,
                nav:false
            },
            576:{
                items:2,
                nav:false
            },
            768:{
                items:3,
                nav:false
            },
            1024:{
                items:4,
                nav:false
            },
            1025:{
                items:5,
                nav:false
            }
        }

    });

    let CareerSlider = $('.CareerSlider > .owl-carousel');
    CareerSlider.owlCarousel({
        items:4,
        loop:true,
        margin:10,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        responsive:{
            0:{
                items:1,
                nav:false
            },
            460:{
                items:2,
                nav:false
            },
            768:{
                items:3,
                nav:false
            },
            1024:{
                items:3,
                nav:false
            },
            1025:{
                items:4,
                nav:false
            }
        }
    });

    let CareerSliders = $('.contentNewsContent > .owl-carousel');
    CareerSliders.owlCarousel({
        loop:true,
        margin:10,
        autoplay:false,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        responsive:{
            0:{
                items:1,
                nav:false
            },
            576:{
                items:1,
                nav:false
            },
            768:{
                items:1.5,
                nav:false
            },
            1024:{
                items:2,
                nav:false
            },
            1025:{
                items:3,
                nav:false
            }
        }
    });

    let CompanySlider = $('.CompanySlider > .owl-carousel');
    CompanySlider.owlCarousel({
        items:3,
        loop:true,
        margin:10,
        autoplay:true,
        autoplayTimeout:2000,
        autoplayHoverPause:true,
        responsive:{
            0:{
                items:1,
                nav:false
            },
            576:{
                items:1,
                nav:false
            },

            1024:{
                items:2,
                nav:false
            },

        }
    });

    let homeSlider = $('.homeSlider > .owl-carousel');
    homeSlider.owlCarousel({
        items:1,
        loop:true,
        margin:0,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:true
    });


    $('.clickBtn').on('click tap', function(){
       $('.clickBtn').removeClass('uk-slidernav-active');
       $(this).addClass('uk-slidernav-active');
    });

    $("ul.ulCategories").on("click", "li:not(.ilCategories)", function() {
        $(this)
            .addClass("ilCategories")
            .siblings()
            .removeClass("ilCategories")
            .closest("div.contentVacancies")
            .find("div.postCategoriesItm")
            .removeClass("ilCategories")
            .eq($(this).index())
            .addClass("ilCategories");
    });

    $('.itmLocation').on('click tap', function () {
        $('.itmLocation').removeClass('itmLocationActiv');
        $(this).addClass('itmLocationActiv');
        $('.postBoxMap').removeClass('postBoxMapActive');
        $('.postBoxMap').eq($(this).index()).addClass('postBoxMapActive')
    });

    $('.itmToggle').on('click tap', function () {
        $(this).closest('.toggleBox').find(this).toggleClass('itmToggleActive');
    });
    $('.btnFormClick').on('click tap', function () {
        let jobTitle = $(this).closest('.toggleBox').find('.titleVacancy').text();
        $(this).closest('.postCategoriesItm').find('.submitForm').find('.selectCategory').text(jobTitle);
        $(this).closest('.postCategoriesItm').find('.submitForm').find('.selectCategory').val(jobTitle);
    });
    $('.upButton').click(function () {
        $('html, body').stop().animate({scrollTop: 0}, 600);
    });
    $('.btn').on('click tap', function () {
        $('.popupElement').addClass('popupElementActive');
        $('html, body').addClass('overflowHidden');
        let valueTitle = $(this).closest('.specification').find('.names').text();
        let valueDescription = $(this).closest('.specification').find('.position').text();
        let nameDesc = $(this).closest('.specification').find('.nameDesc').text();

        let titlePopup = $(this).closest('body').find('.popupElement').find('.descriptionPopup').find('.lastname').text(valueTitle);
        let functionPopup = $(this).closest('body').find('.popupElement').find('.descriptionPopup').find('.function').text(valueDescription);
        let nameDescPopup = $(this).closest('body').find('.popupElement').find('.descriptionPopup').find('.circumscribing').text(nameDesc);
    });
    $('.closePopup, .blackClose').on('click tap', function () {
        $('.popupElement').removeClass('popupElementActive');
        $('html, body').removeClass('overflowHidden');
    });


    $(window).scroll(function (e) {
        let height = $(window).scrollTop();
        if (height >= 485) {
            $('.postCategories').addClass('postCategoriesFixed')
            $('.uk-navbar-container').addClass('uk-navbar-container-active')
        } else {
            $('.uk-navbar-container').removeClass('uk-navbar-container-active')
            $('.postCategories').removeClass('postCategoriesFixed')
        }
    });
    //
    // $(window).scroll(function(){
    //
    //     let docscroll = $(document).scrollTop();
    //     if(docscroll > $(window).height()){
    //         $('.postCategories').addClass('postCategoriesActive');
    //         $('.expertiseItm').addClass('marginLeft');
    //         $('.postCategoriesItm').addClass('marginLeft')
    //
    //         // $('nav').css({'height': $('nav').height(),'width': $('nav').width()}).addClass('postCategories');
    //     }else{
    //         $('.postCategories').removeClass('postCategoriesActive');
    //         $('.expertiseItm').removeClass('marginLeft');
    //         $('.postCategoriesItm').removeClass('marginLeft')
    //     }
    // });
    $('.postCategories').on('click tap', function () {
        console.log('s')
        $('.ulCategories').toggleClass('ulCategoriesActive');

    });

    AOS.init();


    if ($('.pageAbout').length > 0) {
        new TypeIt("#element", {
            strings: "We know about consistency, achievement and dedication. We don’t wait for success, we work for it.",
            speed: 80,
            loop: true
        }).go();
    }

    $(".enevtItm").on('click tap', function () {
        $(".enevtItm").removeClass('enevtItmActive');
        $(this).addClass('enevtItmActive');

    });
    $(".itm-position-e").on('click tap', function () {
        $(".itm-position-e").removeClass('itm-position-e-a');
        $(this).addClass('itm-position-e-a');
        $(".category-general").removeClass('category-general-active');
        $(".category-general").eq($(this).index()).addClass('category-general-active')

    });

    $(".title-category").on('click tap', function () {
        $(this).closest(".category-post").toggleClass('category-post-active');
    });

    $(".show-more").on('click tap', function () {
        if($('.category-post').hasClass('category-post-active')){
            $(this).closest(".category-post").find('.content-category').toggleClass('content-category-active');
        }
    });

    $(".category-itm").on('click tap', function () {
        $(this).find('.checked').toggleClass('checked-active');
    });
    //
    // $(".category-itm-all").on('click tap', function () {
    //     $(this).closest(".category-post").find('.checked').addClass('checked-active');
    //     if($('.checked').hasClass('checked-active')){
    //         $(this).closest(".category-post").find('.checked').removeClass('checked-active');
    //
    //         // $(".category-itm-all-remove").on('click tap', function () {
    //         //
    //         //
    //         // });
    //         console.log('yes')
    //     }else{
    //         $(this).closest(".category-post").find('p').text('Unselect All')
    //         console.log('no')
    //     }
    //
    // });

    $(".category-itm-all").on('click', function (e) {
        let target = $($($(this).find('.input-checkbox'))[0]),
           targetState = target[0].checked, input = $('.category-itm input');
        console.log($(input).val(false));

        if (targetState) {

            $(this).closest(".category-post").find('.checked').addClass('checked-active');
            $(target).siblings('p').text('Unselect all');
            $(input).siblings('checked').removeClass('checked-active');
            $(input).val(false);
        } else {
            $(this).closest(".category-post").find('.checked').removeClass('checked-active');
            $(target).siblings('p').text('Select all');
            $(input).val(false);
            $(input).siblings('checked').addClass('checked-active');
        }
    });

});
