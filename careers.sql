create table category_career (
    id bigint not null auto_increment,
    active bit,
    name varchar(255),
    description varchar(255),
    primary key (id)
);

create table vacancy (
    id bigint not null auto_increment,
    active bit,
    post_date date,
    title varchar(255),
    category_id bigint,
    primary key (id)
);

create table vacancy_requirements (
    id bigint not null auto_increment,
    description varchar(255),
    vacancy_id bigint,
    primary key (id)
);

alter table vacancy add constraint FKj9mac0ovl8bgrjae7ce64022i foreign key (category_id) references category_career (id);
alter table vacancy_requirements add constraint FKkvklu23vis9gi980l6w9slbtc foreign key (vacancy_id) references vacancy (id);
alter table vacancy add column description_position text not null;
